//
//  main.m
//  Tap&Serve
//
//  Created by admin on 11/16/15.
//  Copyright __MyCompanyName__ 2015. All rights reserved.
//

#import <UIKit/UIKit.h>

int main(int argc, char *argv[]) {
    
    NSAutoreleasePool * pool = [[NSAutoreleasePool alloc] init];
    int retVal = UIApplicationMain(argc, argv, nil, @"AppController");
    [pool release];
    return retVal;
}
